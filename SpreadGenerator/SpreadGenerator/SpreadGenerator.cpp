
// Prevents deprecation warnings
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <list>

#include "SpreadGenerator.h"

#define LAYER_FILENAME_LENGTH	256

#define MARKER_FILEPATH			"C:\\Public\\Devel\\SpreadGenerator\\SpreadGenerator\\STagMarkers\\HD17\\"
#define MARKER_FILENAME			""
#define MARKER_FILEEXTENSION	".png"

//#define LAYER_DISPLAY_BBOX

//#define LAYER_BLANK
//#define LAYER_SPINE				
//#define LAYER_COVER				
#define LAYER_DOUBLEPAGE		
#define LAYER_DOUBLEPAGE_EDGE		
//#define LAYER_DOUBLEPAGE_BACKGROUND		
//#define LAYER_DOUBLEPAGE_TOPOLOGY

// Shapes
// ----------------------------------------------------------------------------------------------------------

BaseShape::BaseShape(void)
{
	ReferenceCount = 0;
}

/*virtual*/BaseShape::~BaseShape(void)
{
}

ShapeRect::ShapeRect(const float &fTop, const float &fBottom, const float &fLeft, const float &fRight) :
	BaseShape()
{
	Set(fTop, fBottom, fLeft, fRight);
}

/*virtual*/ShapeRect::~ShapeRect(void)
{
}

/*virtual*/void ShapeRect::RenderToImage(Image *pImage,const float &fPixelsPerInch)
{
	if (pImage != NULL)
	{
		const double dRange = (double)QuantumRange / 256.0;

		const Quantum fillRed = Quantum(Red * dRange);
		const Quantum fillGreen = Quantum(Green * dRange);
		const Quantum fillBlue = Quantum(Blue * dRange);
		const Quantum fillAlpha = Quantum(Alpha * dRange);
		pImage->strokeWidth(0);
		pImage->fillColor(Color(fillRed, fillGreen, fillBlue, fillAlpha));

		int iTop = (int)(Top * fPixelsPerInch);
		int iBottom = (int)(Bottom * fPixelsPerInch);
		int iLeft = (int)(Left * fPixelsPerInch);
		int iRight = (int)(Right * fPixelsPerInch);
		pImage->draw(DrawableRectangle(iLeft,iTop,iRight,iBottom));
	}
}

/*virtual*/ void ShapeRect::Set(const float &fTop, const float &fBottom, const float &fLeft, const float &fRight)
{
	// Only set the boundaries
	Top = fTop;
	Bottom = fBottom;
	Left = fLeft;
	Right = fRight;
}

ShapeMarker::ShapeMarker(const float &fX, const float &fY, const float &fWidth, const float &fHeight) : 
	BaseShape(),
	Scale(1.f)
{
	Set(fX,fY,fWidth,fHeight);
}

/*virtual*/ShapeMarker::~ShapeMarker(void)
{
}

/*virtual*/void ShapeMarker::RenderToImage(Image *pImage, const float &fPixelsPerInch)
{
	if (pImage != NULL)
	{
		const double dRange = (double)QuantumRange / 256.0;

		const Quantum fillRed = Quantum(Red * dRange);
		const Quantum fillGreen = Quantum(Green * dRange);
		const Quantum fillBlue = Quantum(Blue * dRange);
		const Quantum fillAlpha = Quantum(Alpha * dRange);
		pImage->strokeWidth(0);
		pImage->fillColor(Color(fillRed, fillGreen, fillBlue, fillAlpha));

		int iX = (int)(X * fPixelsPerInch);
		int iY = (int)(Y * fPixelsPerInch);
		int iWidth = (int)(Width * fPixelsPerInch * Scale);
		int iHeight = (int)(Height * fPixelsPerInch * Scale);
		pImage->draw(DrawableRectangle(iX, iY, iX+iWidth, iY+iHeight));

#ifdef LAYER_DISPLAY_BBOX
		const Quantum strokeFull = Quantum(255 * dRange);
		pImage->strokeWidth(1);
		pImage->strokeColor(Color(strokeFull, 0.0, 0.0, strokeFull));
		pImage->fillColor(Color(0.0, 0.0, 0.0, 0.0));

		int iTop = (int)(Top * fPixelsPerInch);
		int iBottom = (int)(Bottom * fPixelsPerInch);
		int iLeft = (int)(Left * fPixelsPerInch);
		int iRight = (int)(Right * fPixelsPerInch);
		pImage->draw(DrawableRectangle(iLeft, iTop, iRight, iBottom));
#endif
	}
}

/*virtual*/void ShapeMarker::Set(const float &fX, const float &fY, const float &fWidth, const float &fHeight)
{
	X = fX;
	Y = fY;
	Width = fWidth * Scale;
	Height = fHeight * Scale;
}

/*virtual*/ void ShapeMarker::SetBoundary(const float &fTop, const float &fBottom, const float &fLeft, const float &fRight)
{
	Top = fTop;
	Bottom = fBottom;
	Left = fLeft;
	Right = fRight;

	UpdatePosition();
};

ImageMarker::ImageMarker(const char *pFilename, const float &fX, const float &fY, const float &fWidth, const float &fHeight) : ShapeMarker(fX,fY,fWidth,fHeight)
{
	try
	{
		printf("INFO: Load image %s.\n", pFilename);
		Marker.read(pFilename);
	}
	catch (exception &error)
	{
		printf("ERROR: ImageMagick exception (%s).\n", error.what());
	}
}

/*virtual*/ImageMarker::~ImageMarker(void)
{
}


/*virtual*/void ImageMarker::RenderToImage(Image *pImage, const float &fPixelsPerInch)
{
	if (pImage != NULL)
	{
		int iX = (int)(X * fPixelsPerInch);
		int iY = (int)(Y * fPixelsPerInch);
		int iWidth = (int)(Width * fPixelsPerInch * Scale);
		int iHeight = (int)(Height * fPixelsPerInch * Scale);
		pImage->draw(DrawableCompositeImage(iX, iY, iWidth, iHeight, Marker));

#ifdef LAYER_DISPLAY_BBOX
		const double dRange = (double)QuantumRange / 256.0;

		const Quantum strokeFull = Quantum(255 * dRange);
		pImage->strokeWidth(1);
		pImage->strokeColor(Color(strokeFull, 0.0, 0.0, strokeFull));
		pImage->fillColor(Color(0.0, 0.0, 0.0, 0.0));

		int iTop = (int)(Top * fPixelsPerInch);
		int iBottom = (int)(Bottom * fPixelsPerInch);
		int iLeft = (int)(Left * fPixelsPerInch);
		int iRight = (int)(Right * fPixelsPerInch);
		pImage->draw(DrawableRectangle(iLeft, iTop, iRight, iBottom));
#endif
	}
}

/*virtual*/ void ImageMarker::SetBoundary(const float &fTop, const float &fBottom, const float &fLeft, const float &fRight)
{
	ShapeMarker::SetBoundary(fTop, fBottom, fLeft, fRight);

	UpdateRotation();
}

void ImageMarker::PrepareSTagMarker(const float &fTop, const float &fBottom, const float &fLeft, const float &fRight)
{
	// Hide the marker info
	const double dRange = (double)QuantumRange / 256.0;

	const Quantum fillBlack = Quantum(0.0 * dRange);
	const Quantum fillAlpha = Quantum(256.0 * dRange);
	Marker.strokeWidth(0);
	Marker.fillColor(Color(fillBlack, fillBlack, fillBlack, fillAlpha));

	//Mask the triangle
	CoordinateList coordinates;
	coordinates.push_back(Coordinate((double)fLeft,(double)fTop));
	coordinates.push_back(Coordinate((double)(fLeft+(fRight-fLeft)*0.43f),(double)fTop));
	coordinates.push_back(Coordinate((double)fLeft,(double)(fTop+(fBottom-fTop)*0.43f)));
	Marker.draw(DrawablePolygon(coordinates));

	// Crop the white frame
	int iCropWidth = (int)(fRight - fLeft);
	int iCropHeight = (int)(fBottom - fTop);
	Marker.crop(Geometry(iCropWidth,iCropHeight,(size_t)fLeft,(size_t)fTop));

	// Rotate the marker
	Marker.rotate(Rotation);

	// Invert colors
	Marker.type(Magick::GrayscaleType);
	Marker.negate();

	// Middle grey
	Marker.modulate(73.54, 100.0, 100.0);
}

// Layers
// ----------------------------------------------------------------------------------------------------------

BaseLayer::BaseLayer(const float &fDimensionWidth, const float &fDimensionHeight, const int &iNbMarkers /*= 0*/)
{
	DimensionWidth = fDimensionWidth;
	DimensionHeight = fDimensionHeight;

	NbMarkersMax = iNbMarkers;
	if (NbMarkersMax > 0)
	{
		ppMarkers = (BaseShape**)malloc(NbMarkersMax * sizeof(BaseShape*));
		for (unsigned int i = 0; i < NbMarkersMax; ++i)
		{
			ppMarkers[i] = NULL;
		}
		pOrderedIndexes = (int*)malloc(NbMarkersMax * sizeof(int));
		for (unsigned int i = 0; i < NbMarkersMax; ++i)
		{
			pOrderedIndexes[i] = i;
		}
	}
	else
	{
		ppMarkers = NULL;
		pOrderedIndexes = NULL;
	}
	NbMarkers = 0;
}

/*virtual*/BaseLayer::~BaseLayer(void)
{
	for (unsigned int i = 0; i < NbMarkers; ++i)
	{
		if (ppMarkers[i] != NULL)
		{
			if (ppMarkers[i]->DecReferenceCount() == 0)
			{
				delete ppMarkers[i];
			}
			ppMarkers[i] = NULL;
		}
	}
	NbMarkers = 0;
	free(ppMarkers);
	ppMarkers = NULL;
	free(pOrderedIndexes);
	pOrderedIndexes = NULL;
	NbMarkersMax = 0;
}

bool BaseLayer::AddShape(BaseShape *pShape)
{
	if (pShape != NULL && NbMarkers < NbMarkersMax)
	{
		ppMarkers[NbMarkers++] = pShape;
		pShape->IncReferenceCount();

		return true;
	}
	return false;
}

/*virtual*/void BaseLayer::RenderToImage(Image *pImage)
{
	if (pImage != NULL)
	{
		for (unsigned int i = 0; i < NbMarkers; ++i)
		{
			ppMarkers[pOrderedIndexes[i]]->RenderToImage(pImage, PixelsPerInch);
		}
	}
};

BlankLayer::BlankLayer(const float &fDimensionWidth, const float &fDimensionHeight, const int &iNbMarkers /*= 0*/) : BaseLayer(fDimensionWidth, fDimensionHeight, iNbMarkers)
{
}

/*virtual*/BlankLayer::~BlankLayer(void)
{
}

EdgeLayer::EdgeLayer(const float &fDimensionWidth, const float &fDimensionHeight, const int &iNbMarkers /*= 0*/) : BaseLayer(fDimensionWidth,fDimensionHeight,iNbMarkers)
{
}

/*virtual*/EdgeLayer::~EdgeLayer(void)
{
}

BackgroundLayer::BackgroundLayer(const float &fDimensionWidth, const float &fDimensionHeight, const int &iNbMarkers /*= 0*/) : BaseLayer(fDimensionWidth, fDimensionHeight, iNbMarkers)
{
}

/*virtual*/BackgroundLayer::~BackgroundLayer(void)
{
}

DoubleTrackerLayer::DoubleTrackerLayer(const float &fDimensionWidth, const float &fDimensionHeight, const int &iNbMarkers /*= 0*/) : BaseLayer(fDimensionWidth, fDimensionHeight, iNbMarkers)
{
}

/*virtual*/DoubleTrackerLayer::~DoubleTrackerLayer(void)
{
}

SingleTrackerLayer::SingleTrackerLayer(const float &fDimensionWidth, const float &fDimensionHeight, const int &iNbMarkers/*= 0*/) : BaseLayer(fDimensionWidth, fDimensionHeight, iNbMarkers)
{
}

/*virtual*/SingleTrackerLayer::~SingleTrackerLayer(void)
{
}

SpineTrackerLayer::SpineTrackerLayer(const float &fDimensionWidth, const float &fDimensionHeight, const int &iNbMarkers/*= 0*/) : BaseLayer(fDimensionWidth, fDimensionHeight, iNbMarkers)
{
}

/*virtual*/SpineTrackerLayer::~SpineTrackerLayer(void)
{
}

void Book::PrepareDoublePageEdgeLayers(void)
{
	// Edge layers
	for (int i = 0; i < 4; ++i)
	{
		if (pDoublePageEdgeLayers[i] == NULL)
		{
			pDoublePageEdgeLayers[i] = new EdgeLayer(Definition.DoublePageWidth + Definition.TabWidth * 2.f, Definition.PageHeight, 5);
		}
		pDoublePageEdgeLayers[i]->SetPixelFormat(300.f, 8.f);

		// Frame
		ShapeRect *pShape = new ShapeRect(Definition.InsideBlankSize,
			Definition.InsideBlankSize + Definition.EdgeSize,
			Definition.TabWidth + Definition.InsideBlankSize,
			Definition.TabWidth + Definition.DoublePageWidth - Definition.InsideBlankSize);
		pShape->SetColor(Definition.EdgeColors[i][0], Definition.EdgeColors[i][1], Definition.EdgeColors[i][2], 255);
		pDoublePageEdgeLayers[i]->AddShape(pShape);

		pShape = new ShapeRect(Definition.PageHeight - Definition.InsideBlankSize - Definition.EdgeSize,
			Definition.PageHeight - Definition.InsideBlankSize,
			Definition.TabWidth + Definition.InsideBlankSize,
			Definition.TabWidth + Definition.DoublePageWidth - Definition.InsideBlankSize);
		pShape->SetColor(Definition.EdgeColors[i][0], Definition.EdgeColors[i][1], Definition.EdgeColors[i][2], 255);
		pDoublePageEdgeLayers[i]->AddShape(pShape);

		pShape = new ShapeRect(Definition.InsideBlankSize + Definition.EdgeSize,
			Definition.PageHeight - Definition.InsideBlankSize - Definition.EdgeSize,
			Definition.TabWidth + Definition.InsideBlankSize,
			Definition.TabWidth + Definition.InsideBlankSize + Definition.EdgeSize);
		pShape->SetColor(Definition.EdgeColors[i][0], Definition.EdgeColors[i][1], Definition.EdgeColors[i][2], 255);
		pDoublePageEdgeLayers[i]->AddShape(pShape);

		pShape = new ShapeRect(Definition.InsideBlankSize + Definition.EdgeSize,
			Definition.PageHeight - Definition.InsideBlankSize - Definition.EdgeSize,
			Definition.TabWidth + Definition.DoublePageWidth - Definition.InsideBlankSize - Definition.EdgeSize,
			Definition.TabWidth + Definition.DoublePageWidth - Definition.InsideBlankSize);
		pShape->SetColor(Definition.EdgeColors[i][0], Definition.EdgeColors[i][1], Definition.EdgeColors[i][2], 255);
		pDoublePageEdgeLayers[i]->AddShape(pShape);

		// Central line
		pShape = new ShapeRect(Definition.InsideBlankSize + Definition.EdgeSize,
			Definition.PageHeight - Definition.InsideBlankSize - Definition.EdgeSize,
			Definition.TabWidth + Definition.DoublePageWidth/2.f - Definition.EdgeMiddleLineSize/2.f,
			Definition.TabWidth + Definition.DoublePageWidth/2.f + Definition.EdgeMiddleLineSize/2.f);
		pShape->SetColor(Definition.BrightColor[0], Definition.BrightColor[1], Definition.BrightColor[2], 255);
		pDoublePageEdgeLayers[i]->AddShape(pShape);

	}
}

void Book::PrepareDoublePageBackgroundLayer(void)
{
	// Setup background layer
	if (pDoublePageBackgroundLayer == NULL)
	{
		pDoublePageBackgroundLayer = new BackgroundLayer(Definition.DoublePageWidth + Definition.TabWidth * 2.f, Definition.PageHeight, 1);
	}
	pDoublePageBackgroundLayer->SetPixelFormat(300.f, 8.f);

	ShapeRect *pShape = new ShapeRect(0.0f, Definition.PageHeight, Definition.TabWidth, Definition.TabWidth + Definition.DoublePageWidth);
	pShape->SetColor(0, 0, 0, 255);
	pDoublePageBackgroundLayer->AddShape(pShape);
}

Book::Book(void) :
	pBlankLayer(NULL),
	pSpineBackgroundLayer(NULL),
	pSpineLayer(NULL),
	pCoverBackgroundLayer(NULL),
	pDoublePageBackgroundLayer(NULL)
{
	// Clear arrays of pointers
	for (unsigned int i = 0; i < LAYER_NB_COVERS; ++i)
	{
		pCoverLayers[i] = NULL;
	}

	for (unsigned int i = 0; i < LAYER_NB_EDGES; ++i)
	{
		pDoublePageEdgeLayers[i] = NULL;
	}

	for (unsigned int i = 0; i < LAYER_NB_DOUBLEPAGES; ++i)
	{
		pDoublePageLayers[i] = NULL;
	}

	for (unsigned int i = 0; i < LAYER_NB_DOUBLEPAGES; ++i)
	{
		for (unsigned int j = 0; j < LAYER_NB_MARKERS_PER_DOUBLEPAGE; ++j)
		{
			pDoublePageMarkerLayers[i][j] = NULL;
		}
	}

	// Constants
	Definition.TabWidth = 0.f * CM_TO_INCH;
	Definition.PageWidth = 15.1f * CM_TO_INCH;
	Definition.DoublePageWidth = Definition.PageWidth * 2.f;
	Definition.CoverWidth = 16.7f * CM_TO_INCH;
	Definition.CoverHeight = 25.8f * CM_TO_INCH;
	Definition.SpineWidth = 3.7f * CM_TO_INCH;
	Definition.PageHeight = 24.8f * CM_TO_INCH;

	Definition.InsideBlankSize = .5f * CM_TO_INCH;
	Definition.EdgeSize = 1.f * CM_TO_INCH;
	Definition.EdgeMiddleLineSize = .25f * CM_TO_INCH;
	Definition.MarginFromEdgeSize = .7f * CM_TO_INCH;
	Definition.MarkerSize = 5.f * CM_TO_INCH;
	Definition.MiddleMarkerScale = 1.f;
	Definition.SpineMarkerSize = 3.f * CM_TO_INCH;

	Definition.CoverMarkerBoundaryWidth = Definition.CoverWidth / 2.f;
	Definition.CoverMarkerBoundaryHeight = Definition.CoverHeight / 3.f;
	Definition.PageMarkerBoundaryWidth = (Definition.PageWidth - Definition.MarginFromEdgeSize * 2.f - Definition.EdgeSize - Definition.InsideBlankSize) / 2.f;
	Definition.PageMarkerBoundaryHeight = (Definition.PageHeight - (Definition.MarginFromEdgeSize + Definition.EdgeSize + Definition.InsideBlankSize) * 2.f) / 3.f;
	Definition.SpineMarkerBoundaryWidth = Definition.SpineWidth;

	// sRGB Edge colours
	Definition.EdgeColors[0][0] = 0xff;	Definition.EdgeColors[0][1] = 0x00;	Definition.EdgeColors[0][2] = 0x00;
	Definition.EdgeColors[1][0] = 0x0f;	Definition.EdgeColors[1][1] = 0xd7;	Definition.EdgeColors[1][2] = 0x00;
	Definition.EdgeColors[2][0] = 0xff;	Definition.EdgeColors[2][1] = 0xd2;	Definition.EdgeColors[2][2] = 0x00;
	Definition.EdgeColors[3][0] = 0x86;	Definition.EdgeColors[3][1] = 0x00;	Definition.EdgeColors[3][2] = 0xff;

	Definition.BrightColor[0] = Definition.BrightColor[1] = Definition.BrightColor[2] = 0xbc;
}

Book::~Book(void)
{
	if (pBlankLayer != NULL)
	{
		delete pBlankLayer;
		pBlankLayer = NULL;
	}

	if (pSpineBackgroundLayer != NULL)
	{
		delete pSpineBackgroundLayer;
		pSpineBackgroundLayer = NULL;
	}

	if (pSpineLayer != NULL)
	{
		delete pSpineLayer;
		pSpineLayer = NULL;
	}

	if (pCoverBackgroundLayer != NULL)
	{
		delete pCoverBackgroundLayer;
		pCoverBackgroundLayer = NULL;
	}

	if (pDoublePageBackgroundLayer != NULL)
	{
		delete pDoublePageBackgroundLayer;
		pDoublePageBackgroundLayer = NULL;
	}

	for (unsigned int i = 0; i < LAYER_NB_COVERS; ++i)
	{
		if (pCoverLayers[i] != NULL)
		{
			delete pCoverLayers[i];
			pCoverLayers[i] = NULL;
		}
	}

	for (unsigned int i = 0; i < LAYER_NB_EDGES; ++i)
	{
		if (pDoublePageEdgeLayers[i] == NULL)
		{
			delete pDoublePageEdgeLayers[i];
			pDoublePageEdgeLayers[i] = NULL;
		}
	}

	for (unsigned int i = 0; i < LAYER_NB_DOUBLEPAGES; ++i)
	{
		if (pDoublePageLayers[i] == NULL)
		{
			delete	pDoublePageLayers[i];
			pDoublePageLayers[i] = NULL;
		}
	}

	for (unsigned int i = 0; i < LAYER_NB_DOUBLEPAGES; ++i)
	{
		for (unsigned int j = 0; j < LAYER_NB_MARKERS_PER_DOUBLEPAGE; ++j)
		{
			if (pDoublePageMarkerLayers[i][j] == NULL)
			{
				delete pDoublePageMarkerLayers[i][j];
				pDoublePageMarkerLayers[i][j] = NULL;
			}
		}
	}
}

void Book::PrepareBlankLayer(void)
{
	// Setup blank layer
	if (pBlankLayer == NULL)
	{
		pBlankLayer = new BlankLayer(Definition.DoublePageWidth + Definition.TabWidth * 2.f, Definition.PageHeight, 4);
	}
	pBlankLayer->SetPixelFormat(300.f, 8.f);

	// Allocate space for tabs
	BaseShape *pShape = new ShapeRect(0.f, Definition.PageHeight, 0.f, Definition.TabWidth);
	pShape->SetColor(0, 0, 0, 255);
	pBlankLayer->AddShape(pShape);
	
	pShape = new ShapeRect(0.f, Definition.PageHeight, Definition.TabWidth + Definition.DoublePageWidth, Definition.DoublePageWidth + Definition.TabWidth * 2.f);
	pShape->SetColor(0, 0, 0, 255);
	pBlankLayer->AddShape(pShape);
}

void Book::PrepareSpine(void)
{
	// Setup the background 
	if (pSpineBackgroundLayer == NULL)
	{
		pSpineBackgroundLayer = new BackgroundLayer(Definition.SpineWidth, Definition.CoverHeight, 1);
	}
	pSpineBackgroundLayer->SetPixelFormat(300.f, 8.f);

	ShapeRect *pShape = new ShapeRect(0.f, Definition.CoverHeight, 0.f, Definition.SpineWidth);
	pShape->SetColor(255, 255, 0, 255);
	pSpineBackgroundLayer->AddShape(pShape);

	// Setup the markers
	if (pSpineLayer == NULL)
	{
		pSpineLayer = new SpineTrackerLayer(Definition.SpineWidth, Definition.CoverHeight, 2);
	}
	pSpineLayer->SetPixelFormat(300.f, 8.f);

	float fX = 0.f, fY = 0.f, fStepX = Definition.SpineMarkerBoundaryWidth, fStepY = Definition.CoverMarkerBoundaryHeight;
	for (unsigned int i = 0; i < 2; ++i)
	{
		ShapeMarker *pMarker = new ShapeMarker(0.f, 0.f, Definition.SpineMarkerSize, Definition.SpineMarkerSize);
		pMarker->SetColor(0, 255, 255, 255);
		pMarker->SetBoundary(fY, fY + fStepY, fX, fX + fStepX);
		pSpineLayer->AddShape(pMarker);

		fX += fStepX;
		if ((fX - Definition.SpineWidth) >= NEG_EPSILON)
		{
			fX = 0.f;
			fY += fStepY * 2.f;
		}
	}
}

void Book::PrepareCovers(void)
{
	// Setup background layer
	if (pCoverBackgroundLayer == NULL)
	{
		pCoverBackgroundLayer = new	BackgroundLayer(Definition.CoverWidth, Definition.CoverHeight, 1);
	}
	pCoverBackgroundLayer->SetPixelFormat(300.f, 8.f);

	ShapeRect *pShape = new ShapeRect(0.f, Definition.CoverHeight, 0.f, Definition.CoverWidth);
	pShape->SetColor(255, 255, 0, 255);
	pCoverBackgroundLayer->AddShape(pShape);

	// Setup markers
	for (unsigned int j = 0; j < LAYER_NB_COVERS; ++j)
	{
		if (pCoverLayers[j] == NULL)
		{
			pCoverLayers[j] = new SingleTrackerLayer(Definition.CoverWidth, Definition.CoverWidth, 5);
		}
		pCoverLayers[j]->SetPixelFormat(300.f, 8.f);

		// Setup single page
		float fX = 0.f, fY = 0.f, fStepY = Definition.CoverMarkerBoundaryHeight;
		unsigned int iRow = 0;
		for (unsigned int i = 0; i < 5; ++i)
		{
			float fStepX = Definition.CoverMarkerBoundaryWidth + (iRow & 1) * Definition.CoverMarkerBoundaryWidth / 2.f;

			ShapeMarker *pMarker = new ShapeMarker(0.f, 0.f, Definition.MarkerSize, Definition.MarkerSize);
			pMarker->SetColor(0, 255, 255, 255);
			pMarker->SetBoundary(fY, fY + fStepY, fX, fX + fStepX);
			pCoverLayers[j]->AddShape(pMarker);

			fX += fStepX;
			if ((fX - Definition.CoverWidth + fStepX / 2.f) >= NEG_EPSILON)
			{
				++iRow;
				fX = 0.f;
				fY += fStepY;
			}
		}
		ShapeMarker *pMarkerTarget = (ShapeMarker*)pCoverLayers[j]->GetShape(2);
		while (!(pMarkerTarget->IsAligned((ShapeMarker*)pCoverLayers[j]->GetShape(0), (ShapeMarker*)pCoverLayers[j]->GetShape(3)) ||
			pMarkerTarget->IsAligned((ShapeMarker*)pCoverLayers[j]->GetShape(0), (ShapeMarker*)pCoverLayers[j]->GetShape(4)) ||
			pMarkerTarget->IsAligned((ShapeMarker*)pCoverLayers[j]->GetShape(1), (ShapeMarker*)pCoverLayers[j]->GetShape(3)) ||
			pMarkerTarget->IsAligned((ShapeMarker*)pCoverLayers[j]->GetShape(1), (ShapeMarker*)pCoverLayers[j]->GetShape(4))))
		{
			pMarkerTarget->UpdatePosition();
		}
	}
}

void Book::PrepareDoublePages_LayoutA(void)
{
	// Setup edges
#ifdef LAYER_DOUBLEPAGE_EDGE
	PrepareDoublePageEdgeLayers();
#endif

	// Setup background
#ifdef LAYER_DOUBLEPAGE_BACKGROUND
	PrepareDoublePageBackgroundLayer();
#endif

	char sFilename[LAYER_FILENAME_LENGTH];

	// Generate the marker images
	unsigned int iMarkerCount = 0, iPageCount = 0;
	for (unsigned int j = 0; j < LAYER_NB_DOUBLEPAGES; ++j)
	{
		// Create the independant marker layers
		for (unsigned int i = 0; i < LAYER_NB_MARKERS_PER_DOUBLEPAGE; ++i)
		{
			if (pDoublePageMarkerLayers[j][i] == NULL)
			{
				pDoublePageMarkerLayers[j][i] = new DoubleTrackerLayer(Definition.DoublePageWidth + Definition.TabWidth * 2.f, Definition.PageHeight, 1);
			}
			pDoublePageMarkerLayers[j][i]->SetPixelFormat(300.f, 8.f);
		}

		// Create the double page layer
		if (pDoublePageLayers[j] == NULL)
		{
			pDoublePageLayers[j] = new DoubleTrackerLayer(Definition.DoublePageWidth + Definition.TabWidth * 2.f, Definition.PageHeight, 10);
		}
		pDoublePageLayers[j]->SetPixelFormat(300.f, 8.f);

		// Setup double page
		float fX = Definition.TabWidth + Definition.InsideBlankSize + Definition.EdgeSize + Definition.MarginFromEdgeSize;
		float fY = Definition.InsideBlankSize + Definition.EdgeSize + Definition.MarginFromEdgeSize;
		float fStepY = Definition.PageMarkerBoundaryHeight;
		unsigned int iRow = 0;
		for (unsigned int i = 0; i < (LAYER_NB_MARKERS_PER_DOUBLEPAGE>>1); ++i)
		{
			float fStepX = Definition.PageMarkerBoundaryWidth + (iRow & 1) * Definition.PageMarkerBoundaryWidth / 2.f;

			sprintf(sFilename, "%s%s%05i%s", MARKER_FILEPATH, MARKER_FILENAME, ++iMarkerCount, MARKER_FILEEXTENSION);

			ImageMarker *pMarker = new ImageMarker(sFilename, 0.f, 0.f, Definition.MarkerSize, Definition.MarkerSize);
			pMarker->SetColor(0, 255, 255, 255);
			pMarker->SetBoundary(fY, fY + fStepY, fX, fX + fStepX);
//			pMarker->PrepareSTagMarker(100, 900, 100, 900);

			pDoublePageMarkerLayers[j][i]->AddShape(pMarker);
			pDoublePageLayers[j]->AddShape(pMarker);

			fX += fStepX;
			if ((fX - Definition.PageWidth - Definition.TabWidth + fStepX / 2.f) >= NEG_EPSILON)
			{
				++iRow;
				fX = Definition.TabWidth + Definition.InsideBlankSize + Definition.EdgeSize + Definition.MarginFromEdgeSize;
				fY += fStepY;
			}
		}
		ImageMarker *pMarker = (ImageMarker*)pDoublePageLayers[j]->GetShape(2);
		while (!(pMarker->IsAligned((ShapeMarker*)pDoublePageLayers[j]->GetShape(0), (ShapeMarker*)pDoublePageLayers[j]->GetShape(3)) ||
			pMarker->IsAligned((ShapeMarker*)pDoublePageLayers[j]->GetShape(0), (ShapeMarker*)pDoublePageLayers[j]->GetShape(4)) ||
			pMarker->IsAligned((ShapeMarker*)pDoublePageLayers[j]->GetShape(1), (ShapeMarker*)pDoublePageLayers[j]->GetShape(3)) ||
			pMarker->IsAligned((ShapeMarker*)pDoublePageLayers[j]->GetShape(1), (ShapeMarker*)pDoublePageLayers[j]->GetShape(4))))
		{
			pMarker->UpdatePosition();
		}

		fX = Definition.TabWidth + Definition.PageWidth;
		fY = Definition.InsideBlankSize + Definition.EdgeSize + Definition.MarginFromEdgeSize;
		fStepY = Definition.PageMarkerBoundaryHeight;
		iRow = 0;
		for (unsigned int i = (LAYER_NB_MARKERS_PER_DOUBLEPAGE>>1); i < LAYER_NB_MARKERS_PER_DOUBLEPAGE; ++i)
		{
			float fStepX = Definition.PageMarkerBoundaryWidth + (iRow & 1) * Definition.PageMarkerBoundaryWidth / 2.f;

			sprintf(sFilename, "%s%s%05i%s", MARKER_FILEPATH, MARKER_FILENAME, ++iMarkerCount, MARKER_FILEEXTENSION);

			ImageMarker *pMarker = new ImageMarker(sFilename, 0.f, 0.f, Definition.MarkerSize, Definition.MarkerSize);
			pMarker->SetColor(0, 255, 255, 255);
			pMarker->SetBoundary(fY, fY + fStepY, fX, fX + fStepX);
//			pMarker->PrepareSTagMarker(100, 900, 100, 900);

			pDoublePageMarkerLayers[j][i]->AddShape(pMarker);
			pDoublePageLayers[j]->AddShape(pMarker);

			fX += fStepX;
			if ((fX - Definition.DoublePageWidth - Definition.TabWidth + Definition.InsideBlankSize + Definition.EdgeSize + Definition.MarginFromEdgeSize + fStepX / 2.f) >= NEG_EPSILON)
			{
				++iRow;
				fX = Definition.TabWidth + Definition.PageWidth + (iRow & 1) * fStepX / 2.f;
				fY += fStepY;
			}
		}
		pMarker = (ImageMarker*)pDoublePageLayers[j]->GetShape(7);
		while (!(pMarker->IsAligned((ShapeMarker*)pDoublePageLayers[j]->GetShape(5), (ShapeMarker*)pDoublePageLayers[j]->GetShape(9)) ||
				 pMarker->IsAligned((ShapeMarker*)pDoublePageLayers[j]->GetShape(5), (ShapeMarker*)pDoublePageLayers[j]->GetShape(8)) ||
				 pMarker->IsAligned((ShapeMarker*)pDoublePageLayers[j]->GetShape(6), (ShapeMarker*)pDoublePageLayers[j]->GetShape(9)) ||
				 pMarker->IsAligned((ShapeMarker*)pDoublePageLayers[j]->GetShape(6), (ShapeMarker*)pDoublePageLayers[j]->GetShape(8))))
		{
			pMarker->UpdatePosition();
		}

		// Prepare the markers
		for (unsigned int i = 0; i < LAYER_NB_MARKERS_PER_DOUBLEPAGE; ++i)
		{
			pMarker = (ImageMarker*)pDoublePageLayers[j]->GetShape(i);
			pMarker->PrepareSTagMarker(100, 900, 100, 900);
		}
	}
}

void Book::PrepareDoublePages_LayoutB(void)
{
	// Setup edges
#ifdef LAYER_DOUBLEPAGE_EDGE
	PrepareDoublePageEdgeLayers();
#endif

	// Setup background
#ifdef LAYER_DOUBLEPAGE_BACKGROUND
	PrepareDoublePageBackgroundLayer();
#endif

	char sFilename[LAYER_FILENAME_LENGTH];

	// Generate the marker images
	unsigned int iMarkerCount = 0, iPageCount = 0;
	for (unsigned int j = 0; j < LAYER_NB_DOUBLEPAGES; ++j)
	{
		// Create the independant marker layers
		for (unsigned int i = 0; i < LAYER_NB_MARKERS_PER_DOUBLEPAGE; ++i)
		{
			if (pDoublePageMarkerLayers[j][i] == NULL)
			{
				pDoublePageMarkerLayers[j][i] = new DoubleTrackerLayer(Definition.DoublePageWidth + Definition.TabWidth * 2.f, Definition.PageHeight, 1);
			}
			pDoublePageMarkerLayers[j][i]->SetPixelFormat(300.f, 8.f);
		}

		// Create the double page layer
		if (pDoublePageLayers[j] == NULL)
		{
			pDoublePageLayers[j] = new DoubleTrackerLayer(Definition.DoublePageWidth + Definition.TabWidth * 2.f, Definition.PageHeight, 10);
		}
		pDoublePageLayers[j]->SetPixelFormat(300.f, 8.f);

		// Tweak orders
		pDoublePageLayers[j]->SwapOrders(2, 0);
		pDoublePageLayers[j]->SwapOrders(7, 5);

		// Setup double page (first page)
		float fX = Definition.TabWidth + Definition.InsideBlankSize + Definition.EdgeSize + Definition.MarginFromEdgeSize;
		float fY = Definition.InsideBlankSize + Definition.EdgeSize + Definition.MarginFromEdgeSize;
		unsigned int iRow = 0;
		for (unsigned int i = 0; i < (LAYER_NB_MARKERS_PER_DOUBLEPAGE >> 1); ++i)
		{
			float fStepX = Definition.PageMarkerBoundaryWidth + (iRow & 1) * Definition.PageMarkerBoundaryWidth; // / 2.f;
			float fStepY = ((iRow+1) & 1) * Definition.MarkerSize + (iRow & 1) * (Definition.PageHeight - (Definition.MarkerSize + Definition.MarginFromEdgeSize + Definition.EdgeSize + Definition.InsideBlankSize) * 2.f);

			sprintf(sFilename, "%s%s%05i%s", MARKER_FILEPATH, MARKER_FILENAME, ++iMarkerCount, MARKER_FILEEXTENSION);

			ImageMarker *pMarker = new ImageMarker(sFilename, 0.f, 0.f, Definition.MarkerSize, Definition.MarkerSize);
			pMarker->SetColor(0, 255, 255, 255);
			pMarker->SetBoundary(fY, fY + fStepY, fX, fX + fStepX);
//			pMarker->PrepareSTagMarker(100, 900, 100, 900);

			pDoublePageMarkerLayers[j][i]->AddShape(pMarker);
			pDoublePageLayers[j]->AddShape(pMarker);

			fX += fStepX;
			if ((fX - Definition.PageWidth - Definition.TabWidth + fStepX / 2.f) >= NEG_EPSILON)
			{
				++iRow;
				fX = Definition.TabWidth + Definition.InsideBlankSize + Definition.EdgeSize + Definition.MarginFromEdgeSize; // +(iRow & 1) * fStepX / 2.f;
				fY += fStepY;
			}
		}
		// Tweak markers' positions, scales and orientations
		ImageMarker *pMarker = (ImageMarker*)pDoublePageLayers[j]->GetShape(0);			
		pMarker->UpdatePositionTopLeft();
		ImageMarker *pMarkerTarget = (ImageMarker*)pDoublePageLayers[j]->GetShape(1);	
		pMarkerTarget->UpdatePositionTopRight();
		while (pMarker->IsOrientedLike(pMarkerTarget))
		{
			pMarker->UpdateRotation();
			pMarkerTarget->UpdateRotation();
		}

		pMarker = (ImageMarker*)pDoublePageLayers[j]->GetShape(2);
		pMarker->SetScale(Definition.MiddleMarkerScale);
		pMarker->UpdatePositionCenterRight();
		pMarker->UpdateRotation();

		pMarker = (ImageMarker*)pDoublePageLayers[j]->GetShape(3);
		pMarker->UpdatePositionBottomLeft();
		pMarkerTarget = (ImageMarker*)pDoublePageLayers[j]->GetShape(4);
		pMarkerTarget->UpdatePositionBottomRight();
		while (pMarker->IsOrientedLike(pMarkerTarget))
		{
			pMarker->UpdateRotation();
			pMarkerTarget->UpdateRotation();
		}

		// Setup double page (second page)
		fX = Definition.TabWidth + Definition.PageWidth + Definition.MarginFromEdgeSize;
		fY = Definition.InsideBlankSize + Definition.EdgeSize + Definition.MarginFromEdgeSize;
		iRow = 0;
		for (unsigned int i = (LAYER_NB_MARKERS_PER_DOUBLEPAGE >> 1); i < LAYER_NB_MARKERS_PER_DOUBLEPAGE; ++i)
		{
			float fStepX = Definition.PageMarkerBoundaryWidth + (iRow & 1) * Definition.PageMarkerBoundaryWidth; // / 2.f;
			float fStepY = ((iRow+1) & 1) * Definition.MarkerSize + (iRow & 1) * (Definition.PageHeight - (Definition.MarkerSize + Definition.MarginFromEdgeSize + Definition.EdgeSize + Definition.InsideBlankSize) * 2.f);

			sprintf(sFilename, "%s%s%05i%s", MARKER_FILEPATH, MARKER_FILENAME, ++iMarkerCount, MARKER_FILEEXTENSION);

			ImageMarker *pMarker = new ImageMarker(sFilename, 0.f, 0.f, Definition.MarkerSize, Definition.MarkerSize);
			pMarker->SetColor(0, 255, 255, 255);
			pMarker->SetBoundary(fY, fY + fStepY, fX, fX + fStepX);
//			pMarker->PrepareSTagMarker(100, 900, 100, 900);

			pDoublePageMarkerLayers[j][i]->AddShape(pMarker);
			pDoublePageLayers[j]->AddShape(pMarker);

			fX += fStepX;
			if ((fX - Definition.DoublePageWidth - Definition.TabWidth + Definition.InsideBlankSize + Definition.EdgeSize + Definition.MarginFromEdgeSize + fStepX / 2.f) >= NEG_EPSILON)
			{
				++iRow;
				fX = Definition.TabWidth + Definition.PageWidth + Definition.MarginFromEdgeSize;// +(iRow & 1) * fStepX / 4.f;
				fY += fStepY;
			}
		}
		// Tweak markers' positions and scale
		pMarker = (ImageMarker*)pDoublePageLayers[j]->GetShape(5);
		pMarker->UpdatePositionTopLeft();
		pMarkerTarget = (ImageMarker*)pDoublePageLayers[j]->GetShape(6);
		pMarkerTarget->UpdatePositionTopRight();
		while (pMarker->IsOrientedLike(pMarkerTarget))
		{
			pMarker->UpdateRotation();
			pMarkerTarget->UpdateRotation();
		}

		pMarker = (ImageMarker*)pDoublePageLayers[j]->GetShape(7);
		pMarker->SetScale(Definition.MiddleMarkerScale);
		pMarker->UpdatePositionCenterLeft();
		pMarker->UpdateRotation();

		pMarker = (ImageMarker*)pDoublePageLayers[j]->GetShape(8);
		pMarker->UpdatePositionBottomLeft();
		pMarkerTarget = (ImageMarker*)pDoublePageLayers[j]->GetShape(9);
		pMarkerTarget->UpdatePositionBottomRight();
		while (pMarker->IsOrientedLike(pMarkerTarget))
		{
			pMarker->UpdateRotation();
			pMarkerTarget->UpdateRotation();
		}

		// Prepare the markers
		for (unsigned int i = 0; i < LAYER_NB_MARKERS_PER_DOUBLEPAGE; ++i)
		{
			pMarker = (ImageMarker*)pDoublePageLayers[j]->GetShape(i);
			pMarker->PrepareSTagMarker(100, 900, 100, 900);
		}
	}
}

// Main
// ----------------------------------------------------------------------------------------------------------

int main(int argc, char *argv[])
{
	Book					book;
	const BookDefinition	bookDefinition = book.GetDefinition();

	// Init
	srand((unsigned int)time(NULL));
	char sFilename[LAYER_FILENAME_LENGTH];

	// Layer definitions
#ifdef LAYER_BLANK
	book.PrepareBlankLayer();
#endif 
#ifdef LAYER_SPINE
	book.PrepareSpine();
#endif
#ifdef LAYER_COVER
	book.PrepareCovers();
#endif
#ifdef LAYER_DOUBLEPAGE
	book.PrepareDoublePages();
#endif

	// Initialize ImageMagick install location for Windows
	InitializeMagick(*argv);

	ColorRGB	color(0.0, 0.0, 0.0, 0.0);

#ifdef LAYER_DOUBLEPAGE
#ifdef LAYER_DOUBLEPAGE_TOPOLOGY
	// Open the topology file
	FILE *fTopologyFile = fopen("topology.txt", "wt");
	if (fTopologyFile == NULL)
	{
		printf("ERROR: Can't create file topology.txt.\n");

		return 0;
	}
#endif
#endif

	// Write data
	try
	{
#ifdef LAYER_BLANK
		// Save Blank layer
		BlankLayer *pBlankLayer = book.GetBlankLayer();
		Image imgBlank(Geometry(pBlankLayer->GetPixelWidth(), pBlankLayer->GetPixelHeight()), color);
		imgBlank.type(MagickCore::TrueColorAlphaType);
		pBlankLayer->RenderToImage(&imgBlank);
		printf("INFO: Saving blank file.\n");
		imgBlank.write("Blank.png");
#endif

#ifdef LAYER_SPINE
		// Save spine
		BackgroundLayer *pSpineBgLayer = book.GetSpineBackgroundLayer();
		Image imgSpnBg(Geometry(pSpineBgLayer->GetPixelWidth(), pSpineBgLayer->GetPixelHeight()), color);
		imgSpnBg.type(MagickCore::TrueColorAlphaType);
		pSpineBgLayer->RenderToImage(&imgSpnBg);
		imgSpnBg.write("spinebackground.png");

		SpineTrackerLayer *pSpineLayer = book.GetSpineLayer();
		Image imgSpine(Geometry(pSpineLayer->GetPixelWidth(), pSpineLayer->GetPixelHeight()), color);
		imgSpine.type(MagickCore::TrueColorAlphaType);
		pSpineLayer->RenderToImage(&imgSpine);
		imgSpine.write("spine.png");
#endif

#ifdef LAYER_COVER
		// Save cover
		BackgroundLayer *pCoverBgLayer = book.GetCoverBackgroundLayer();
		Image imgSglBg(Geometry(pCoverBgLayer->GetPixelWidth(), pCoverBgLayer->GetPixelHeight()), color);
		imgSglBg.type(MagickCore::TrueColorAlphaType);
		pCoverBgLayer->RenderToImage(&imgSglBg);
		imgSglBg.write("coverbackground.png");

		SingleTrackerLayer **pCoverLayers = book.GetCoverLayers();
		for (unsigned int j = 0; j < LAYER_NB_COVERS; ++j)
		{
			Image imgSingle(Geometry(pCoverLayers[j]->GetPixelWidth(), pCoverLayers[j]->GetPixelHeight()), color);
			imgSingle.type(MagickCore::TrueColorAlphaType);
			pCoverLayers[j]->RenderToImage(&imgSingle);
			sprintf(sFilename, "Cover%02i.png", j);
			imgSingle.write(sFilename);
		}
#endif

#ifdef LAYER_DOUBLEPAGE

#ifdef LAYER_DOUBLEPAGE_BACKGROUND
		// Save double page images
		BackgroundLayer *pDoublePageBgLayer = book.GetDoublePageBackgroundLayer();
		Image imgDblBg(Geometry(pDoublePageBgLayer->GetPixelWidth(), pDoublePageBgLayer->GetPixelHeight()), color);
		imgDblBg.type(MagickCore::TrueColorAlphaType);
		pDoublePageBgLayer->RenderToImage(&imgDblBg);
		for (unsigned int j = 0; j < LAYER_NB_DOUBLEPAGES; ++j)
		{
			sprintf(sFilename, "Page%02i&%02i.Background.png", (j << 1) + 1, (j << 1) + 2);

			printf("INFO: Saving background file %s.\n", sFilename);

			imgDblBg.write(sFilename);
		}
#endif

#ifdef LAYER_DOUBLEPAGE_EDGE		
		// Save Double plage edges
		EdgeLayer **pDoublePageEdgeLayers = book.GetDoublePageEdgeLayers();
		for (int i = 0; i < LAYER_NB_EDGES; ++i)
		{
			Image imgEdge(Geometry(pDoublePageEdgeLayers[i]->GetPixelWidth(), pDoublePageEdgeLayers[i]->GetPixelHeight()), color);
			imgEdge.type(MagickCore::TrueColorAlphaType);
			pDoublePageEdgeLayers[i]->RenderToImage(&imgEdge);
			for (unsigned int j = 0; j < LAYER_NB_DOUBLEPAGES; j += LAYER_NB_EDGES)
			{
				sprintf(sFilename, "Page%02i&%02i.Edge.png", ((i + j) << 1) + 1, ((i + j) << 1) + 2);

				printf("INFO: Saving edge file %s.\n", sFilename);

				imgEdge.write(sFilename);
			}
		}
#endif

		// Save the double page
		for (unsigned int i = 0; i < LAYER_NB_DOUBLEPAGES; ++i)
		{
			// Save the double page
			DoubleTrackerLayer **pDoubleTrackerLayer = book.GetDoublePageLayers();
			Image imgDouble(Geometry(pDoubleTrackerLayer[i]->GetPixelWidth(), pDoubleTrackerLayer[i]->GetPixelHeight()), color);
			imgDouble.type(MagickCore::TrueColorAlphaType);
			pDoubleTrackerLayer[i]->RenderToImage(&imgDouble);

			sprintf(sFilename, "Page%02i&%02i.Markers.png", (i << 1) + 1, (i << 1) + 2);

			printf("INFO: Saving marker file %s.\n", sFilename);

			imgDouble.write(sFilename);

			// Save a page per marker
			DoubleTrackerLayer **pDoubleTrackerMarkerLayers = book.GetDoublePageMarkerLayers(i);
			for (unsigned int j = 0; j < LAYER_NB_MARKERS_PER_DOUBLEPAGE; ++j)
			{
				ImageMarker *pMarker = (ImageMarker*)pDoubleTrackerMarkerLayers[j]->GetShape(0);
				float fWidth = pMarker->GetWidth(), fHeight = pMarker->GetHeight(), fX = pMarker->GetX(), fY = pMarker->GetY();
				Image imgDouble(Geometry(pDoubleTrackerMarkerLayers[j]->GetPixelWidth(), pDoubleTrackerMarkerLayers[j]->GetPixelHeight()), color);
				imgDouble.type(MagickCore::TrueColorAlphaType);
				pDoubleTrackerMarkerLayers[j]->RenderToImage(&imgDouble);
				
				sprintf(sFilename, "Page%02i.Marker%i.png", (j > 4 ? (i<<1) + 2: (i<<1) + 1), (j > 4 ? j - 4 : j + 1) );
				
				printf("INFO: Saving marker file %s.\n", sFilename);
				
				imgDouble.write(sFilename);

#ifdef LAYER_DOUBLEPAGE_TOPOLOGY
				// Save the individual marker page
				double dPixelsPerInch = pDoubleTrackerMarkerLayers[j]->GetPixelsPerInch();
				fprintf(fTopologyFile, "Page%02i.Marker%i Size=%ix%i PixelPosition=%i,%i InchPosition=%f,%f MmPosition=%f,%f\n", (j > 4 ? (i<<1) + 2 : (i<<1) + 1), (j > 4 ? j - 4 : j + 1),
					(int)(fWidth*dPixelsPerInch), (int)(fHeight*dPixelsPerInch),
					(int)(fX*dPixelsPerInch), (int)(fY*dPixelsPerInch),
					fX, fY,
					fX * INCH_TO_MM, fY * INCH_TO_MM);
#endif
			}
		}
#endif
	}
	catch (exception &error)
	{
		printf("ERROR: ImageMagick exception (%s)\n", error.what());
	}

#ifdef LAYER_DOUBLEPAGE
#ifdef LAYER_DOUBLEPAGE_TOPOLOGY
	// Close the topology file
	fclose(fTopologyFile);
#endif
#endif

	return 1;
}
