
#ifndef __SPREADGENERATOR__
#define __SPREADGENERATOR__

#include <Magick++.h>

#define LAYER_NB_EDGES					4
#define LAYER_NB_COVERS					2
#define LAYER_NB_DOUBLEPAGES			14
#define LAYER_NB_MARKERS_PER_DOUBLEPAGE	10

#define EPSILON							0.1f
#define NEG_EPSILON						-EPSILON
#define CM_TO_INCH						0.393701f
#define INCH_TO_CM						2.54f
#define INCH_TO_MM						2540.f
#define ALIGNMENT_THRESHOLD				(2.5f * CM_TO_INCH)
#define ORIENTATION_THRESHOLD			7.5f

using namespace std;
using namespace Magick;

enum ePixelFormat
{
	PIXELFORMAT_15,
	PIXELFORMAT_16,
	PIXELFORMAT_24,
	PIXELFORMAT_32,
};

// Shapes
// ----------------------------------------------------------------------------------------------------------

class BaseShape
{
public:

	BaseShape(void);
	virtual ~BaseShape(void);

	inline int IncReferenceCount(void);
	inline int DecReferenceCount(void);

	inline void SetColor(const int &iR, const int &iG, const int &iB, const int &iA);

	virtual void RenderToImage(Image *pImage, const float &fPixelsPerInch) = 0;
	virtual void Set(const float &, const float &, const float &, const float &) = 0;

protected:

	int		ReferenceCount;

	int		Red;
	int		Blue;
	int		Green;
	int		Alpha;

};

class ShapeRect : public BaseShape
{
public:

	ShapeRect(const float &, const float &, const float &, const float &);
	/*virtual*/~ShapeRect(void);

	/*virtual*/void RenderToImage(Image *pImage, const float &fPixelsPerInch);
	/*virtual*/void Set(const float &, const float &, const float &, const float &);

protected:

	float Top;							// Dimensions in inches
	float Bottom;
	float Left;
	float Right;

};

class ShapeMarker : public BaseShape
{
public:

	ShapeMarker(const float &, const float &, const float &, const float &);
	/*virtual*/~ShapeMarker(void);

	/*virtual*/void RenderToImage(Image *pImage, const float &fPixelsPerInch);
	/*virtual*/void Set(const float &, const float &, const float &, const float &);
	/*virtual*/void SetBoundary(const float &, const float &, const float &, const float &);

	inline void	SetScale(const float &fScale);

	inline void UpdatePosition(void);
	inline void UpdatePositionTopLeft(void);
	inline void UpdatePositionTopRight(void);
	inline void UpdatePositionBottomLeft(void);
	inline void UpdatePositionBottomRight(void);
	inline void UpdatePositionCenterLeft(void);
	inline void UpdatePositionCenterRight(void);
	inline void UpdatePositionCenter(void);
	inline bool IsAligned(ShapeMarker*, ShapeMarker*);

	inline float GetX(void);
	inline float GetY(void);
	inline float GetWidth(void);
	inline float GetHeight(void);
	inline float GetScale(void);

protected:

	float Width;						// Dimensions, in inches
	float Height;
	float X;
	float Y;
	float Scale;

	float Top;							// Boundaries, in inches
	float Bottom;
	float Left;
	float Right;

};

class ImageMarker : public ShapeMarker
{
public:

	ImageMarker(const char*, const float &, const float &, const float &, const float &);
	/*virtual*/~ImageMarker(void);

	/*virtual*/void RenderToImage(Image *pImage, const float &fPixelsPerInch);
	/*virtual*/void SetBoundary(const float &, const float &, const float &, const float &);

	void PrepareSTagMarker(const float &, const float &, const float &, const float &);

	inline void UpdateRotation(void);
	inline bool IsOrientedLike(ImageMarker *pMarker);

	inline double GetRotation(void);

private:

	Image	Marker;

	double	Rotation;

};

// Layers
// ----------------------------------------------------------------------------------------------------------

class BaseLayer
{
public:

	BaseLayer(const float &fDimensionWidth, const float &fDimensionHeight, const int &iNbMarkers = 0);
	virtual ~BaseLayer(void);

	bool				AddShape(BaseShape*);
	inline BaseShape*	GetShape(const unsigned int &iIndex);

	virtual void		RenderToImage(Image *pImage);
	inline void			SetPixelFormat(const float &fPixelsPerInch, const float &fPixelSampleRate);
	inline float		GetWidth(void);
	inline float		GetHeight(void);
	inline float		GetPixelsPerInch(void);
	inline unsigned int GetPixelWidth(void);
	inline unsigned int GetPixelHeight(void);
	inline float		GetSampleRate(void);
	inline unsigned int GetSamplePixelWidth(void);
	inline unsigned int GetSamplePixelHeight(void);
	inline void			SwapOrders(const int &iDst, const int &iSrc);

protected:

	unsigned int	NbMarkersMax;
	unsigned int	NbMarkers;
	BaseShape		**ppMarkers;
	int				*pOrderedIndexes;

	float			DimensionWidth;		// In inches
	float			DimensionHeight;

	float			PixelsPerInch;
	float			PixelSampleRate;

	unsigned int	PixelWidth;
	unsigned int	PixelHeight;
	unsigned int	SamplePixelWidth;
	unsigned int	SamplePixelHeight;

};

class BlankLayer : public BaseLayer
{
public:

	BlankLayer(const float &fDimensionWidth, const float &fDimensionHeight, const int &iNbMarkers = 0);
	~BlankLayer(void);

};

class SpineTrackerLayer : public BaseLayer
{
public:

	SpineTrackerLayer(const float &fDimensionWidth, const float &fDimensionHeight, const int &iNbMarkers);
	~SpineTrackerLayer(void);

private:

};

class EdgeLayer : public BaseLayer
{
public:

	EdgeLayer(const float &fDimensionWidth, const float &fDimensionHeight, const int &iNbMarkers = 0);
	~EdgeLayer(void);

};

class BackgroundLayer : public BaseLayer
{
public:

	BackgroundLayer(const float &fDimensionWidth, const float &fDimensionHeight, const int &iNbMarkers = 0);
	~BackgroundLayer(void);

};

class DoubleTrackerLayer : public BaseLayer
{
public:

	DoubleTrackerLayer(const float &fDimensionWidth, const float &fDimensionHeight, const int &iNbMarkers);
	~DoubleTrackerLayer(void);

private:

};

class SingleTrackerLayer : public BaseLayer
{
public:

	SingleTrackerLayer(const float &fDimensionWidth, const float &fDimensionHeight, const int &iNbMarkers);
	~SingleTrackerLayer(void);

private:

};

// Book
// ----------------------------------------------------------------------------------------------------------

struct BookDefinition
{
	// Constants (in inches)
	float TabWidth;
	float PageWidth;
	float DoublePageWidth;
	float CoverWidth;
	float CoverHeight;
	float SpineWidth;
	float PageHeight;

	float InsideBlankSize;
	float EdgeSize;
	float EdgeMiddleLineSize;
	float MarginFromEdgeSize;
	float MarkerSize;
	float MiddleMarkerScale;
	float SpineMarkerSize;

	float CoverMarkerBoundaryWidth;
	float CoverMarkerBoundaryHeight;
	float PageMarkerBoundaryWidth;
	float PageMarkerBoundaryHeight;
	float SpineMarkerBoundaryWidth;

	// sRGB Edge colours
	unsigned char EdgeColors[4][3];
	unsigned char BrightColor[3];

};

class Book
{
public:

	Book(void);
	~Book(void);

	inline const BookDefinition&	GetDefinition(void);

	inline BlankLayer*				GetBlankLayer(void);

	inline BackgroundLayer*			GetSpineBackgroundLayer(void);
	inline SpineTrackerLayer*		GetSpineLayer(void);

	inline SingleTrackerLayer**		GetCoverLayers(void);
	inline BackgroundLayer*			GetCoverBackgroundLayer(void);

	inline BackgroundLayer*			GetDoublePageBackgroundLayer(void);
	inline EdgeLayer**				GetDoublePageEdgeLayers(void);
	inline DoubleTrackerLayer**		GetDoublePageLayers(void);
	inline DoubleTrackerLayer**		GetDoublePageMarkerLayers(const unsigned int &iIndex);

	void PrepareBlankLayer(void);
	void PrepareSpine(void);
	void PrepareCovers(void);
	inline void PrepareDoublePages(void);


private:

	void PrepareDoublePageEdgeLayers(void);
	void PrepareDoublePageBackgroundLayer(void);
	void PrepareDoublePages_LayoutA(void);
	void PrepareDoublePages_LayoutB(void);

	BookDefinition		Definition;

	BlankLayer			*pBlankLayer;

	BackgroundLayer		*pSpineBackgroundLayer;
	SpineTrackerLayer	*pSpineLayer;

	BackgroundLayer		*pCoverBackgroundLayer;
	SingleTrackerLayer	*pCoverLayers[LAYER_NB_COVERS];

	BackgroundLayer		*pDoublePageBackgroundLayer;
	EdgeLayer			*pDoublePageEdgeLayers[LAYER_NB_EDGES];
	DoubleTrackerLayer	*pDoublePageLayers[LAYER_NB_DOUBLEPAGES];
	DoubleTrackerLayer	*pDoublePageMarkerLayers[LAYER_NB_DOUBLEPAGES][LAYER_NB_MARKERS_PER_DOUBLEPAGE];

};

// Inline
// ----------------------------------------------------------------------------------------------------------

inline int BaseShape::IncReferenceCount(void)
{
	return ++ReferenceCount;

};

inline int BaseShape::DecReferenceCount(void)
{
	return --ReferenceCount;
};

inline void BaseShape::SetColor(const int &iR, const int &iG, const int &iB, const int &iA)
{
	Red = iR;
	Green = iG;
	Blue = iB;
	Alpha = iA;
};

inline void ShapeMarker::SetScale(const float &fScale)
{
	Scale = fScale;
};

inline void ShapeMarker::UpdatePositionTopLeft(void)
{
	X = Left;
	Y = Top;
};

inline void ShapeMarker::UpdatePositionTopRight(void)
{
	X = Right - Width * Scale;
	Y = Top;
};

inline void ShapeMarker::UpdatePositionBottomLeft(void)
{
	X = Left;
	Y = Bottom - Height * Scale;
};

inline void ShapeMarker::UpdatePositionBottomRight(void)
{
	X = Right - Width;
	Y = Bottom - Height * Scale;
};

inline void ShapeMarker::UpdatePositionCenter(void)
{
	X = Left + ((Right - Left) - Width * Scale) / 2.f;
	Y = Top + ((Bottom - Top) - Height * Scale) / 2.f;
};

inline void ShapeMarker::UpdatePositionCenterRight(void)
{
	X = Right - Width;
	Y = Top + ((Bottom - Top) - Height * Scale) / 2.f;
};

inline void ShapeMarker::UpdatePositionCenterLeft(void)
{
	X = Left;
	Y = Top + ((Bottom - Top) - Height * Scale) / 2.f;
};

inline void ShapeMarker::UpdatePosition(void)
{
	X = Left + ((Right - Left) - Width * Scale) * rand() / (float)RAND_MAX;
	Y = Top + ((Bottom - Top) - Height * Scale) * rand() / (float)RAND_MAX;
};

inline bool ShapeMarker::IsAligned(ShapeMarker *pMarker1, ShapeMarker *pMarker2)
{
	float x1 = pMarker1->GetX(), y1 = pMarker1->GetY(), x2 = pMarker2->GetX(), y2 = pMarker2->GetY();
	float a = y1 - y2, b = x2 - x1, c = x1 * y2 - x2 * y1;

	return fabs(a * X + b * Y + c) / sqrtf(a * a + b * b) < ALIGNMENT_THRESHOLD;
};

inline float ShapeMarker::GetX(void)
{
	return X;
};

inline float ShapeMarker::GetY(void)
{
	return Y;
};

inline float ShapeMarker::GetWidth(void)
{
	return Width;
};

inline float ShapeMarker::GetHeight(void)
{
	return Height;
};

inline float ShapeMarker::GetScale(void)
{
	return Scale;
};

inline void ImageMarker::UpdateRotation(void)
{
	const double dRange = 15.0;
	Rotation = (dRange*rand()) / RAND_MAX + (45.0 - dRange/2.0);
};

inline double ImageMarker::GetRotation(void)
{
	return Rotation;
};

inline bool ImageMarker::IsOrientedLike(ImageMarker *pMarker)
{
	if (fabs(Rotation - pMarker->GetRotation()) < ORIENTATION_THRESHOLD )
	{
		return true;
	}
	return false;
};

inline BaseShape* BaseLayer::GetShape(const unsigned int &iIndex)
{
	if( iIndex < NbMarkers)
	{
		return ppMarkers[iIndex];
	}
	return NULL;
};

inline void BaseLayer::SetPixelFormat(const float &fPixelsPerInch,const float &fPixelSampleRate)
{
	PixelsPerInch = fPixelsPerInch;
	PixelSampleRate = fPixelSampleRate;

	PixelWidth = (unsigned int)(DimensionWidth * PixelsPerInch);
	PixelHeight = (unsigned int)(DimensionHeight * PixelsPerInch);
	SamplePixelWidth = (unsigned int)(DimensionWidth * PixelsPerInch / PixelSampleRate);
	SamplePixelHeight = (unsigned int)(DimensionHeight * PixelsPerInch / PixelSampleRate);
};

inline float BaseLayer::GetPixelsPerInch(void)
{
	return PixelsPerInch;
};

inline float BaseLayer::GetSampleRate(void)
{
	return PixelSampleRate;
};

inline float BaseLayer::GetWidth(void)
{
	return DimensionWidth;
};

inline float BaseLayer::GetHeight(void)
{
	return DimensionHeight;
};

inline unsigned int BaseLayer::GetPixelWidth(void)
{
	return PixelWidth;
};

inline unsigned int BaseLayer::GetPixelHeight(void)
{
	return PixelHeight;
};

inline unsigned int BaseLayer::GetSamplePixelWidth(void)
{
	return SamplePixelWidth;
};

inline unsigned int BaseLayer::GetSamplePixelHeight(void)
{
	return SamplePixelHeight;
};

inline void	BaseLayer::SwapOrders(const int &iDst, const int &iSrc)
{
	int iSwap = pOrderedIndexes[iSrc];
	pOrderedIndexes[iSrc] = pOrderedIndexes[iDst];
	pOrderedIndexes[iDst] = iSwap;
};

inline const BookDefinition& Book::GetDefinition(void)
{
	return Definition;
};

inline BlankLayer* Book::GetBlankLayer(void)
{
	return pBlankLayer;
};

inline BackgroundLayer* Book::GetSpineBackgroundLayer(void)
{
	return pSpineBackgroundLayer;
};

inline SpineTrackerLayer* Book::GetSpineLayer(void)
{
	return pSpineLayer;
};

inline SingleTrackerLayer** Book::GetCoverLayers(void)
{
	return pCoverLayers;
};

inline BackgroundLayer* Book::GetCoverBackgroundLayer(void)
{
	return pCoverBackgroundLayer;
};

inline BackgroundLayer* Book::GetDoublePageBackgroundLayer(void)
{
	return pDoublePageBackgroundLayer;
};

inline EdgeLayer** Book::GetDoublePageEdgeLayers(void)
{
	return pDoublePageEdgeLayers;
};

inline DoubleTrackerLayer** Book::GetDoublePageLayers(void)
{
	return pDoublePageLayers;
};

inline DoubleTrackerLayer** Book::GetDoublePageMarkerLayers(const unsigned int &iIndex)
{
	return pDoublePageMarkerLayers[iIndex];
};

inline void Book::PrepareDoublePages(void)
{
	PrepareDoublePages_LayoutB();
};

#endif
